# Copyright (C) 2018, 2020-2023 |Méso|Star> (contact@meso-star.com)
# Copyright (C) 2018 Centre National de la Recherche Scientifique
# Copyright (C) 2018 Université Paul Sabatier
#
# This program is free software: you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.
#
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
# GNU General Public License for more details.
#
# You should have received a copy of the GNU General Public License
# along with this program. If not, see <http://www.gnu.org/licenses/>.

.POSIX:
.SUFFIXES: # Clean up default inference rules

include config.mk

LIBNAME_STATIC = libhtmie.a
LIBNAME_SHARED = libhtmie.so
LIBNAME = $(LIBNAME_$(LIB_TYPE))

################################################################################
# Library building
################################################################################
SRC = src/htmie.c
OBJ = $(SRC:.c=.o)
DEP = $(SRC:.c=.d)

build_library: .config $(DEP)
	@$(MAKE) -fMakefile $$(for i in $(DEP); do echo -f $${i}; done) \
	$$(if [ -n "$(LIBNAME)" ]; then \
	     echo "$(LIBNAME)"; \
	   else \
	     echo "$(LIBNAME_SHARED)"; \
	   fi)

$(DEP) $(OBJ): config.mk

$(LIBNAME_SHARED): $(OBJ)
	$(CC) $(CFLAGS_SO) $(DPDC_CFLAGS) -o $@ $(OBJ) $(LDFLAGS_SO) $(DPDC_LIBS)

$(LIBNAME_STATIC): libhtmie.o
	$(AR) -rc $@ $?
	$(RANLIB) $@

libhtmie.o: $(OBJ)
	$(LD) -r $(OBJ) -o $@
	$(OBJCOPY) $(OCPFLAGS) $@

.config: config.mk
	@if ! $(PKG_CONFIG) --atleast-version $(NETCDF_VERSION) netcdf; then \
	  echo "netcdf $(NETCDF_VERSION) not found" >&2; exit 1; fi
	@if ! $(PKG_CONFIG) --atleast-version $(RSYS_VERSION) rsys; then \
	  echo "rsys $(RSYS_VERSION) not found" >&2; exit 1; fi
	@echo "config done" > $@

.SUFFIXES: .c .d .o
.c.d:
	@$(CC) $(CFLAGS_SO) $(DPDC_CFLAGS) -MM -MT "$(@:.d=.o) $@" $< -MF $@

.c.o:
	$(CC) $(CFLAGS_SO) $(DPDC_CFLAGS) -DHTMIE_SHARED_BUILD -c $< -o $@

################################################################################
# Installation
################################################################################
pkg:
	sed -e 's#@PREFIX@#$(PREFIX)#g'\
	    -e 's#@VERSION@#$(VERSION)#g'\
	    -e 's#@NETCDF_VERSION@#$(NETCDF_VERSION)#g'\
	    -e 's#@RSYS_VERSION@#$(RSYS_VERSION)#g'\
	    htmie.pc.in > htmie.pc

htmie-local.pc: htmie.pc.in
	sed -e '1d'\
	    -e 's#^includedir=.*#includedir=./src/#'\
	    -e 's#^libdir=.*#libdir=./#'\
	    -e 's#@VERSION@#$(VERSION)#g'\
	    -e 's#@NETCDF_VERSION@#$(NETCDF_VERSION)#g'\
	    -e 's#@RSYS_VERSION@#$(RSYS_VERSION)#g'\
	    htmie.pc.in > $@

install: build_library pkg
	@$(SHELL) make.sh install "$(DESTDIR)$(PREFIX)/lib" $(LIBNAME)
	@$(SHELL) make.sh install "$(DESTDIR)$(PREFIX)/lib/pkgconfig" htmie.pc
	@$(SHELL) make.sh install "$(DESTDIR)$(PREFIX)/include/high_tune" src/htmie.h
	@$(SHELL) make.sh install "$(DESTDIR)$(PREFIX)/share/doc/htmie" COPYING README.md
	@$(SHELL) make.sh install "$(DESTDIR)$(PREFIX)/share/man/man5" htmie.5

uninstall:
	rm -f "$(DESTDIR)$(PREFIX)/lib/$(LIBNAME)"
	rm -f "$(DESTDIR)$(PREFIX)/lib/pkgconfig/htmie.pc"
	rm -f "$(DESTDIR)$(PREFIX)/share/doc/htmie/COPYING"
	rm -f "$(DESTDIR)$(PREFIX)/share/doc/htmie/README.md"
	rm -f "$(DESTDIR)$(PREFIX)/include/high_tune/htmie.h"
	rm -f "$(DESTDIR)$(PREFIX)/share/man/man5/htmie.5"

################################################################################
# Miscellaneous targets
################################################################################
all: build_library build_tests

clean: clean_test
	rm -f $(OBJ) $(TEST_OBJ) $(LIBNAME)
	rm -f .config .test libhtmie.o htmie.pc htmie-local.pc

distclean: clean
	rm -f $(DEP) $(TEST_DEP)

lint:
	shellcheck -o all make.sh
	shellcheck -o all src/dump_netcdf_data.sh
	mandoc -Tlint -Wall htmie.5 || [ $$? -le 1 ]

################################################################################
# Tests
################################################################################
TEST_SRC = src/test_htmie.c src/test_htmie_load.c
TEST_OBJ = $(TEST_SRC:.c=.o)
TEST_DEP = $(TEST_SRC:.c=.d)

TEST_MIE_LUT = Mie_LUT_Cloud
TEST_MIE_LUT_CDL = $(TEST_MIE_LUT).cdl
TEST_MIE_LUT_NETCDF = $(TEST_MIE_LUT).nc
TEST_MIE_LUT_VARS =\
 $(TEST_MIE_LUT).g\
 $(TEST_MIE_LUT).lambda\
 $(TEST_MIE_LUT).macs\
 $(TEST_MIE_LUT).mscs

TEST_DUMMY = dummy
TEST_DUMMY_CDL = $(TEST_DUMMY).cdl
TEST_DUMMY_NETCDF = $(TEST_DUMMY).nc
TEST_DUMMY_VARS =\
 $(TEST_DUMMY).g\
 $(TEST_DUMMY).lambda\
 $(TEST_DUMMY).macs\
 $(TEST_DUMMY).mscs

PKG_CONFIG_LOCAL = PKG_CONFIG_PATH="./:$${PKG_CONFIG_PATH}" $(PKG_CONFIG)
HTMIE_CFLAGS = $$($(PKG_CONFIG_LOCAL) $(PCFLAGS) --cflags htmie-local.pc)
HTMIE_LIBS = $$($(PKG_CONFIG_LOCAL) $(PCFLAGS) --libs htmie-local.pc)

test: $(TEST_MIE_LUT_VARS) $(TEST_MIE_LUT_NETCDF)
test: $(TEST_DUMMY_VARS) $(TEST_DUMMY_NETCDF)
test: build_tests
	@$(SHELL) make.sh check test_htmie test_htmie
	@$(SHELL) make.sh check test_htmie_load_LUT test_htmie_load Mie_LUT_Cloud.nc .
	@$(SHELL) make.sh check test_htmie_load_dummy test_htmie_load dummy.nc .

.SUFFIXES: .cdl .nc .g .lambda .macs .mscs
.cdl.nc:
	ncgen -o $@ $<

.nc.g .nc.lambda .nc.macs .nc.mscs:
	$(SHELL) src/dump_netcdf_data.sh $$(p="$@"; echo "$${p##*.}") $< > $@

build_tests: build_library $(TEST_DEP) .test $(TEST_MIE_LUT_VARS) $(TEST_DUMMY_VARS)
	@$(MAKE) -fMakefile -f.test $$(for i in $(TEST_DEP); do echo -f"$${i}"; done) test_bin

.test: Makefile make.sh
	@$(SHELL) make.sh config_test $(TEST_SRC) > $@

clean_test:
	$(SHELL) make.sh clean_test $(TEST_SRC)
	rm -rf $(TEST_MIE_LUT_VARS) $(TEST_MIE_LUT_NETCDF)
	rm -rf $(TEST_DUMMY_VARS) $(TEST_DUMMY_NETCDF)

$(TEST_DEP): config.mk htmie-local.pc
	@$(CC) $(CFLAGS_EXE) $(HTMIE_CFLAGS) $(RSYS_CFLAGS) \
	-MM -MT "$(@:.d=.o) $@" $(@:.d=.c) -MF $@

$(TEST_OBJ): config.mk htmie-local.pc
	$(CC) $(CFLAGS_EXE) $(HTMIE_CFLAGS) $(RSYS_CFLAGS) -c $(@:.o=.c) -o $@

test_htmie test_htmie_load: config.mk htmie-local.pc $(LIBNAME)
	$(CC) $(CFLAGS_EXE) -o $@ src/$@.o $(LDFLAGS_EXE) $(HTMIE_LIBS) $(RSYS_LIBS) -lm
