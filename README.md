# High-Tune: Mie

This C library loads the optical properties of water droplets obtained
by a Mie scattering evaluation and saved in htmie format. See `htmie.5`
for format specification.

## Requirements

- C compiler
- POSIX make
- pkg-config
- netCDF4
- [RSys](https://gitlab.com/vaplv/rsys)
- [mandoc](https://mandoc.bsd.lv)

## Installation

Edit config.mk as needed, then run:

    make clean install

## Release notes

### Version 0.1

- Correction of validity range tests on loaded data: validity ranges
  were incorrect for mass absorption cross sections, mass scattering
  cross sections and Hengyey-Greenstein asymmetric phase function
  parameters.
- Do not manage the netCDF file with git: follow its CDL version
  instead.
- Write the man page directly in mdoc's roff macros, instead of using
  the intermediate scdoc source.
- Replace CMake by Makefile as build system.
- Update compiler and linker flags to increase the security and
  robustness of generated binaries.
- Provide a pkg-config file to link the library as an external
  dependency.

### Version 0.0.4

Use scdoc rather than asciidoc as file format for man sources.

### Version 0.0.3

Sets the CMake minimum version to 3.1: since CMake 3.20, version 2.8 has
become obsolete.

### Version 0.0.2

Fix compilation warnings when using the NetCDF library in version 4.4.0.

### Version 0.0.1

- Fix compilation errors when using the NetCDF library in version 4.4.0.
- Minor correction of linear interpolation: the interpolation parameter
  was not correctly truncated between [0,1] in order to handle numerical
  inaccuracy.

## Copyrights

Copyright (C) 2018, 2020-2023 |Méso|Star> (contact@meso-star.com)  
Copyright (C) 2018 Centre National de la Recherche Scientifique (CNRS)  
Copyright (C) 2018 Université Paul Sabatier

## License

HTMie is free software released under the GPL v3+ license: GNU GPL
version 3 or later. You are welcome to redistribute it under certain
conditions; refer to the COPYING file for details.
